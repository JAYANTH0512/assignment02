package com.innominds.assignment;

// 4.Write a Program illustrating Daemon Threads

/**
 * Java program to illustrate the Daemon Threads
 * 
 * @author jpalepally1
 *
 */
public class TestDaemonThread extends Thread {
	
	public TestDaemonThread(String name) {
		super(name);
	}

	public void run() {

		if (Thread.currentThread().isDaemon()) { // Checking Whether thread is Daemon or not
			System.out.println(getName() + "  Daemon Thread");
		} else {
			System.out.println(getName() + "  User Thread");
		}
	}

	public static void main(String[] args) {
		TestDaemonThread t1 = new TestDaemonThread("t1");
		TestDaemonThread t2 = new TestDaemonThread("t2");
		TestDaemonThread t3 = new TestDaemonThread("t3");

		t1.setDaemon(true); // Setting thread t1 into Daemon

		t1.start();
		t2.start();

		t3.setDaemon(true); // Setting thread t3 into Daemon
		t3.start();

	}
}
