package com.innominds.assignment;

// 7.Write a program to sort the any type of data by using inner class without using predefined? 

import java.util.Arrays;

/**
 * Perform a Java Program to sort the input array of data 
 * @author jpalepally1
 *
 */

public class Sorting {

	class DataSorting {
		public int[] dataSorting(int[] a) {
			for (int i = 0; i < a.length; i++) { // Number of Passes
				for (int j = i + 1; j < a.length; j++) { // Iteration in each pass
					if (a[i] > a[j]) {
						int temp = a[i];
						a[i] = a[j];
						a[j] = temp;

					}

				}

			}
			return a;
		}
	}

	/**
	 * Main Method to perform the above class 
	 * @param args
	 */
	public static void main(String[] args) {
		DataSorting obj = new Sorting().new DataSorting();
		int a[] = { 4, 3, 2, 1, 5 };
		System.out.println("Arrays before sorting " + Arrays.toString(a));
		System.out.println("Arrays after sorting " + Arrays.toString(obj.dataSorting(a)));

	}
}
