package com.innominds.employee;

public class Main {

	public static void main(String[] args) {
		  EmployeeDB empDB = new EmployeeDB();
	        Employee emp1 =new Employee(101,"Jayanth","jai@gmail.com",'M',5000);
	        Employee emp2 =new Employee(102,"manu","Manu@gmail.com",'f',4000);
	        Employee emp3 =new Employee(103,"swapna","swapna@gmail.com",'f',100000);
	        Employee emp4 =new Employee(104,"Sai","sai@gmail.com",'M',6000);
	        Employee emp5 =new Employee(105,"Keerthana","Amith@gmail.com",'f',7000);
	        empDB.addEmployee(emp1);
	        empDB.addEmployee(emp2);
	        empDB.addEmployee(emp3);
	        empDB.addEmployee(emp4);
	        empDB.addEmployee(emp5);
	        
	        for(Employee emp:empDB.listAll()) {
	            System.out.println(emp.GetEmployeeDetails());
	        }
	            System.out.println();
	            
	            empDB.deleteEmployee(101);
	            
	            for(Employee emp:empDB.listAll()) {
	                System.out.println(emp.GetEmployeeDetails());
	            }
	                System.out.println();
	                System.out.println(empDB.showPaySlip(103));
	            
	        }
	}
