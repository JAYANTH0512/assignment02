package com.innominds.assignment;

/**
 * 3.Write a java program to create five threads with different priorities. Send two threads of highest priority in sleep state.
 *  Check the aliveness of the threads and mark which dread is long listing.
 * @author jpalepally1
 *
 */

class Mythread extends Thread {
	static int flag; 

	public void run() {
		try {
			if (Thread.currentThread().getPriority() == 10 || Thread.currentThread().getPriority() == 7) {

				if (Thread.currentThread().getPriority() == 10) {
					Thread.sleep(1000);
				} else { 											// if priority is 7
					Thread.sleep(3000);
				}

				System.out.println(Thread.currentThread().getPriority() + " sleeping");
			}

			for (int i = 0; i < 5; i++) {
				System.out.println("Inside run method of priority  " + Thread.currentThread().getPriority());
			}

		} catch (Exception e) {
			System.out.println(e);
		}

		flag = Thread.currentThread().getPriority();
	}
}



public class Question3{
	public static void createThread() {
		try {
			Mythread t1 = new Mythread();
			Mythread t2 = new Mythread();
			Mythread t3 = new Mythread();
			Mythread t4 = new Mythread();
			Mythread t5 = new Mythread();

			t1.setPriority(10);
			t2.setPriority(7);
			t3.setPriority(6);
			t4.setPriority(3);
			t5.setPriority(1);

			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t5.start();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void main(String[] args) throws Exception {

		createThread();
		Thread.sleep(5000); 											// sleeping main thread //
		System.out.println("*******************************   long lasting thread " + Mythread.flag); // print long lasting thread
	}
}

	

